# Breitbandausbau an Sachsens Schulen

Dieses Projekt stellt die Internetverbindungen von Schulen in Sachsen auf einer Karte dar. 
Es soll der Allgemeinheit dienen und eine Hilfestellung für den Breitbandausbau vor allem im Bildungssektor bieten.
Eine erste Version kann man hier [https://schulinternet.sachsen.codefor.de/](https://schulinternet.sachsen.codefor.de/) finden.
Vielen dank an die [OKFN](https://okfn.de/).

![Screenshot](screenshot.png)

## Ausgangslage
Der Breitbandausbau in Sachsen hinkt extrem hinterher. Kaum irgendwo wird das deutlicher sichtbar als im Bildungsbereich. Viele Schulen arbeiten im Jahr 2018 immer noch mit Modemverbindungen. In diesem Zusammenhang wurde eine kleine Anfrage (von Kirsten Muster, Die Blauen) and die Sächsische Staatsregierung gestellt. Über das traurige Ergebnis berichtet auch die [LVZ](http://www.lvz.de/Region/Mitteldeutschland/Sachsens-Schulen-hinken-hinterher). Da nun [65 Seiten PDFs](./data/6_Drs_13106_1_1_1_.pdf) nicht unbedingt der beste Weg ist, Sachverhalte für die Öffentlichkeit darzulegen, möchten wir hier eine Kartenanwendung bauen, die einen intuitiveren und leichten Zugang zum Thema vermittelt. Da die Ergebisse der Anfrage keine Geokoordinaten enthalten, müssen die Daten mit der [sächsischen Schuldatenbank](https://www.schuldatenbank.sachsen.de/index.php) zusammengeführt werden. 

## Aktueller Stand
- Die Daten wurden mit dem Tool Tabula (Geheimtipp zur Extraktion von Strukturierten Informatione aus PDFs) aus der Antwort auf die Anfrage extrahiert und leicht nachbearbeitet. Das Ergebniss wurde im [CSV Format](sources/schoolsWithInternet.csv) bzw. im [JSON Format](sources/schoolsWithInternet.json) abgelegt.
- Aus der Schuldatenbank wurden die HTML Beschreibungen aller in der Datenbank befindlichen Schulprotraits am 30.05.2018 gecrawlt und im `results` Ordner gespeichert. Außerdem wurde noch ein minimales [Cheerio](https://cheerio.js.org/) Script angelegt, um aus dem eher schlecht strukturierten HTML, neben den Geokoordinaten zusätzliche Informationen zu filtern.
- Das Ergebnis dieser beiden Schritte ist also eine JSON Datei bei der Schulen Namen und Geokoordinaten (2320 Einträge) haben und eine JSON Datei bei der Schulen Namen und Internetbandbreite (1543 Einträge) haben.
- In einem ersten sehr redimentären Schritt wurden einfach die Namen auf Gleichheit verglichen. Dabei wurden 873 Übereinstimmungen getroffen, die aber in keinster weise validiert wurden.
- Die Schulen wurden dann als GeoJSON gespeichert und die verschiedenen Internetbandbreiten werden in `schools.html` in verschiedenen Farben auf einer Karte dargestellt.

## Weitere Arbeiten
- Matching der Namen muss noch über weitere im PDF verfügbar Parameter funktionieren
- Es sollte möglich sein verschiedene Layer (Grundschule, Gymnasium) auszuwählen
- Es sollte möglich sein nach verschiedenen Kriterien (Bandbreite, Kreis etc.) zu filtern
- Es sollte die Möglichkeit geben eine Detailseite (erstmal als Popup) für die Schulen einzusehen, je mehr Werte von der Schuldatenbank extrahiert werden können umso besser
- Veröffentlichung der Arbeit

## Verwendete Software, DANKE!
- [Tabula](https://tabula.technology/)
- [cheerio.js](https://cheerio.js.org/)