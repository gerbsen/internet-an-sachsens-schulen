var fs = require('fs');
var csv = require("fast-csv");
var stream = fs.createReadStream("/Users/gerb/Development/private/internet-an-sachsens-schulen/sources/schoolsWithInternet.csv");
var schoolsWithGps = require('../sources/schoolsWithoutInternet.json');
var NGrams = require('../lib/ngrams.js');

var n = new NGrams()

var speeds = {};

var schoolTypes = new Set([]);
var matches = [];
var csvStream = csv({'delimiter':';'})
    .on("data", function(row){

        var schoolWithInternet = {};
        schoolWithInternet.id = row[0];
        schoolWithInternet.name = row[1];
        schoolWithInternet.note = row[2];
        schoolWithInternet.city = row[3];
        schoolWithInternet.district = row[4];
        schoolWithInternet.internet = row[5];
        
        var bestMatch = {
            similarity : 0,
            schoolWithGps : ""
        }

        speeds[row[5]] = speeds[row[5]] ? speeds[row[5]] + 1 : 1;

        var nameNgram = new NGrams(2);
        nameNgram.add(schoolWithInternet.name);
        var cityNgram = new NGrams(2);
        cityNgram.add(schoolWithInternet.city);

        schoolsWithGps.forEach(function(schoolWithGps){

            schoolTypes.add(schoolWithGps.image_url);

            var totalScore = 0;
            var scores = nameNgram.search(schoolWithGps.name);
            if ( scores.length == 1 ) {
                totalScore += scores[0].similarity;
            }
            else {
                // console.log(schoolWithInternet.name + " ---- " + schoolWithGps.name);
            }

            var scores = cityNgram.search(schoolWithGps.city);
            if ( scores.length == 1 ) {
                totalScore += scores[0].similarity;
            }
            else {
                // console.log(schoolWithInternet.name + " ---- " + schoolWithGps.name);
            }

            if ( totalScore > bestMatch.similarity ) {

                bestMatch.schoolWithGps = schoolWithGps;
                bestMatch.similarity    = totalScore;
            }
        })

        bestMatch.schoolWithGps.internet = schoolWithInternet.internet;
        bestMatch.schoolWithGps.similarity = bestMatch.similarity;
        matches.push(bestMatch.schoolWithGps);
    })
    .on("end", function(){

        console.log(schoolTypes);
        console.log(speeds);
        console.log("Found matches: " + matches.length);

        var features = [];
        matches.forEach(function(school){

            if ( school.internet ) {

                features.push({
                    "type": "Feature",
                    "geometry" : {
                        "type": "Point",
                        "coordinates": [school.lng, school.lat],
                    },
                    "properties" : {
                        "title" : school.title,
                        "description" : school.description,
                        "imageUrl" : school.image_url,
                        "internet" : school.internet,
                        "similarity" : school.similarity,
                        "city" : school.city,
                        "id" : school.id,
                        "postal_code": school.postal_code
                    }
                });    
            }
        });

        var featureCollection = {
            "type": "FeatureCollection",
            "features": features
        }

        // { Internetanbindung: 1,
        //   '1 Mbit/s (DSL 1000)': 60,
        //   '128 Mbit/s (DSL 128000)': 21,
        //   '16 Mbit/s (DSL 16000)': 409,
        //   '2 Mbit/s (DSL 2000)': 140,
        //   '32 Mbit/s (DSL 32000)': 67,
        //   '6 Mbit/s (DSL 6000)': 286,
        //   '64 Mbit/s (DSL 64000)': 58,
        //   ISDN: 159,
        //   'keine Angabe': 97,
        //   Modem: 88,
        //   schneller: 18,
        //   UMTS: 14 }

        fs.writeFileSync("/Users/gerb/Development/private/internet-an-sachsens-schulen/results/geojson_schools.json", "var schools = " + JSON.stringify(featureCollection, null, 4) + ";", function(err) {
            if(err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
    });

stream.pipe(csvStream);
